import { createApp } from 'vue'

import App from './App.vue'
import router from './router'
import SocketIO from 'socket.io-client'
import VueSocketIOExt from 'vue-socket.io-extended';
import { createStore } from 'vuex'
import VuePlyr from 'vue-plyr'
import 'vue-plyr/dist/vue-plyr.css'
import VueI18n from 'vue-i18n'


const app = createApp(App);
app.use(VuePlyr, {
  plyr: {}
})
const i18n = new VueI18n({
  locale: 'en-US',
  formatter: new CustomFormatter(/* here the constructor options */),
  messages: {
    'en-US': {
      // ...
    },
    // ...
  }
})
app.use(i18n)

// Create a new store instance.
const store = createStore({
  state() {
    return {
      streamsId: [],
      chat: false
    }
  },

})
app.use(store)


const socket = SocketIO('http://localhost:1337/'
);
app.use(VueSocketIOExt, socket);


import OpenLayersMap from 'vue3-openlayers'
import 'vue3-openlayers/dist/vue3-openlayers.css'

app.use(OpenLayersMap)
app.use(router)
app.mount('#app')





